class MaterialPoint2D extends Point2D{
    private double mass

    MaterialPoint2D(double x, double y, double mass) {
        super(x, y)
        this.mass = mass
    }

    double getMass() {
        mass
    }

    void setMass(double mass) {
        this.mass = mass
    }


    String toString() {
        "x = " + getX()+
                " y = " + getY() +
                " masa = "+ mass
    }
}
