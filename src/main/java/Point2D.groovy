class Point2D {
    private double x
    private double y

    Point2D(){}

    Point2D(double x,double y){
        this.x=x
        this.y=y
    }

    double getX() {
        x
    }

    void setX(double x) {
        this.x = x
    }

    double getY() {
        y
    }

    void setY(double y) {
        this.y = y
    }

    String toString() {
        "x = " + x + ", y = "+ y
    }
}
